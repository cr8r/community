---
title: About Us
---

# About CR8R

Welcome to the CR8R community.

If you wish to submit an issue, idea or suggestion regarding this site or the Mastodon instance at [CR8R.gg](https://cr8r.gg). Methods to do so are, by either: sending a direct post (DM) on Mastodon to [@CR8R](https://cr8r.gg/CR8R), emailing [support@cr8r.gg](mailto:support@cr8r.gg) or using our [GitLab Issue Tracker](https://gitlab.com/cr8r/community/-/issues/new).

## Mission

> The underlying objective of the CR8R community is to be a friendly, respectful and inclusive place for all types of creators, those who support creative freedoms and those who like to appreciate and consume those creations one way or another.

> You are absolutely free to come join the instance and embark on journey into the fediverse.

_Please Note: We do not limit registration based on language or location, please keep in mind that this instance is hosted in the UK so your experience may vary._

-----

As a general-purpose instance (albeit with a creator theme), we do not have heavy moderation in terms of what topics people are allowed to post about, however all users are expected to follow our rules at all times, and generally be nice and friendly on the federation.

Please report all content you see which might violate our rules for evaluation. If you are on a remote server, please forward any reports of our users to our server for our moderators to take action, we pledge that remote reports will remain confidential within our moderation team and will not be used for any form of retribution against the reporter.

For more information: [cr8r.io](https://cr8r.io)

-----

## Mastodon Server Covenant

While this instance is relatively new, we do intend to keep it running long-term and it follow the Server Covenant as outlined at [joinmastodon.org/covenant](https://joinmastodon.org/covenant).

* We agree to active moderation against (but not limited to): racism, sexism, homophobia and transphobia

* We agree to daily backups to ensure your data is safe

* We have backup administrators ready to operate this server in emergencies or unexpected absence

* In the unlikely event this instance shuts down, we agree to give our users a 3 month period to migrate to another instance

-----

CR8R is being run as a non-profit organisation and will be incorporated as such at the earliest opportunity.