---
title: Our Rules
---

# Rules

These are the rules that are currently in operation on our instance.
We regularly review them and any changes will be documented here as well as announced on the instance.

* Be Kind and Respectful. Don't be racist, sexist, homophobic, transphobic, ableist, fatphobic, etc.

* Any discriminatory, threatening or harassment behaviour or content promoting / advocating said behaviours will not be tolerated.

* Keep Your Content LEGAL! Don't do / discuss doing / link to anything illegal (as determined by UK & EU law).

* Must consider others at all times. Include relevant #HashTags to aid discoverability & filtering potential and add image descriptions (Alt-Text).

* Use of a Content Wrapper/Warning (CW) must be utilised for topics and/or images that may invoke a strong difference of opinion or could be distressing to some (for example: phobias), as well as lewd, suggestive or adult themed imagery or text.

* Credit other artists / photographers / creators appropriately when posting their work whenever possible.

* No unsolicited / spam posts permitted. This includes mass mentioning, timeline flooding, constant follow/boost requests, commercial/product advertising or promotional campaigns, etc.

* No impersonation of an entity or another individual. Your identity must be your own, whether this is yourself or your online persona. Business / corporate / automated / bot accounts are discouraged from joining unless for support purposes only.

* Do not misuse the reporting, CW or any other systems of the fediverse as well as attempting to disrupt or spoil others' experiences.

---

Wish to seek clarification, suggest new or amend existing rules or just want to open up discussions about them?

Open up an issue on our GitLab repository: [cr8r/community](https://gitlab.com/cr8r/community/-/issues/new)