# Content Moderation

Our wish is that we can be a low moderation instance, however as stated in our rules & detailed within these documents - this is not a “free for all”.

While freedom of expression is indeed a foundational right and as such it is essential for the enjoyment and protection of all human rights, it is not absolute. 
When posts contain material that could be demeaning, disrespectful and/or downright offensive towards individuals or a specific group of, then this right is no longer valid and will be subject to moderation & consequences.

We want the instance to be used by anyone and that they feel safe that they will be able to shield themselves from any sort of content they wish not to see.

## Difference Of Opinions

Individuals aren’t always going to see eye to eye and have differing opinions on a number of topics.
This is totally fine, and somewhat encouraged to foster some healthy debate and conversations.
Users have the control of when to ignore/mute/block other users when they feel that those conversations and those debates become too heated. 
All other parties involved must respect that at all times.

## NSFW / Adult (18+) / Sensitive Material

Any posts with content that is classed as inappropriate for reading/viewing in a professional setting (NSFW), contain topics/images of an adult nature or that may invoke polarised opinions, trauma, phobias, etc must be marked as sensitive with text and/or any appropriate hashtags within the post & its content warning clearly defining what it is.
Doing so would allow individuals to place filters in order to minimise their exposure to unwanted content and have a better experience.
Posts may be marked as sensitive or removed by the Moderation team if they don't adhere to this.

## Hateful / Derogatory / Disrespectful

Any posts or user accounts brought to our attention that contains directly or indirectly (linked) material that encourages or advocates violence or any discrimination against people (individual or as a group) will not be tolerated.

## Unsolicated / Spam

Irrelevant or unsolicited posts (public or not) for the purposes which include (but not limited to): 
- advertising/promotion of a service or product
- mass mentioning
- timeline flooding
Unless permission to post was sought and given, accounts engaging in this activity will be suspended

## Automated / Bot Accounts

To ensure unneccessary 'noise' within the instance (and the fediverse as a whole), automated & bot accounts must follow the following guidelines:
- The account must have checked the profile option "This is an automated account".
- Posts from the bot account must be "unlisted".
- Must enable "Automated post deletion" with a maximum age threshold of one month, with the exception of pinned posts and direct messages. Exceptions based on interactions are not eligible.
Automated / Bot accounts that do not follow the above policy will be suspended.

## Follow Requests / Begging

Please do not engage in posting messages solely for the aim to ask for follows, whether its to everyone, a group of people or one in particular.