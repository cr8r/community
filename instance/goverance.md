---
title: Goverance
---

# Governance

While the day to day operations are the responsibility of the Moderation Team, the instance is governed by those who use and support it.

As the instance grows, requests/suggestions etc can be submitted to help shape our future within our own little community aswell as the wider fediverse.

You can DM [@CR8R](https://cr8r.gg/@CR8R), email [support@cr8r.gg](mailto:support@cr8r.gg) or use our [GitLab Issue Tracker](https://gitlab.com/cr8r/community/-/issues) 

## Moderation Team

### Admins

> [@GunFheum](https://cr8r.gg/@GunFheum)

> [@SmuggieStreams](https://cr8r.gg/@SmuggieStreams)

### Moderators

> _None._

**Want to help & join the team?**

Looking for people to join as Moderators to help with assisting new sign-ups as well as resolving reports and dealing with any unwanted content.

If you think you would be a good fit, then please DM us or send us an email at [support@cr8r.gg](mailto:support@cr8r.gg) and tell us a little about yourself & your past experience.

While we are primarily seeking Moderators, if you have relevant skills that you think may be benefical to the community, then please do get in touch also.