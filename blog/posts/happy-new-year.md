---
date: 2023-01-04
description: >
  Happy New Year & those incidents over the festive period
---

# Happy New Year + Incident Recap!

Firstly, we would like to welcome everyone that has joined so far (now over 450). Very excited to see you all showcase anything creative you made, like, etc.

<!-- more -->

We are absoluately enjoying the variety of creative individuals that chose [CR8R](https://cr8r.gg) as their home on the fediverse. From authors to streamers and from musicians to photographers. Our aim is to be a safe space for ANY type of creator, and not pigeon-hole or segregate different types of creative outlets. 

We hope that during 2023, we'll see more creations & more interaction as user numbers & activity tick upwards on the instance and the wider network in general.

### Incidents over the festive period

Unfortunately, we had a couple of outages with out mail delivery system that caused registration issues as some unlucky users didnt receive their confirmation links upon signup. There were 2 of these incidents which lasted for 24 hours. 

After the first which happened on the 30th December, we thought things were back to normal and connectivity with ourselves & our provider was restored. This incident in turn clogged up the server's disk space resulting in a downtime of 30 minutes, while things were rectified.

However on the 2nd January, we suffered another loss of connectivity with said provider. That was the last straw.

It was not good enough and we can't apologise enough to those affected as well as everyone that uses the instance.

We have now changed our mail delivery service to another provider as we couldn't in good faith keep our previous provider given the issues they were causing. As for the disk space being fully utilised, we don't envisge this to happen again with our switch to a new provider. In saying that, we have put some safe guards in place to minimise this sort of issue happening again.

Again, we whole heartedly apologise for any inconvenience this may have caused and hope that it hasn't swayed you away from our instance or from the fediverse as a whole.
