---
date: 2024-02-15
description: >
  It's been a while
---

# It's been a while since we wrote a blog post

Apologies for the lack of posts over the last year. Planning on posting at least once a month from now on with some updates on what we have seen.

<!-- more -->

This will encompass news & updates from the CR8R team as well as showcasing some of the topics that have had our users interacting.

If you have ideas on what you would like to see within these posts or wish to submit a post yourself, then please let us know.

### As always

Whether you want to help out the moderation team, or request a change to our rules or instance moderation list, send us a DM or email detailing these changes. We are help to discuss things further.

This is your community as much as ours, we want everyone to feel apart of it and help guide CR8R as it grows and develops.

We will be posting again soon. Thank you for your time.