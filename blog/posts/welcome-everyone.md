---
date: 2022-12-24
description: >
  Our new blog to welcome everyone to CR8R & the fediverse
---

# Welcome Everyone!

As some of us wind down and partake in some festive activities, we would just like to welcome everyone that has joined so far (over 300 of you) and of course those who join after this is posted.

<!-- more -->

While the clock ticks down over 2022, plans are being drafted up detailing our next steps in 2023. If there is any idea/suggestion you wish to be added/considered then please contact us via email [support@cr8r.gg], DM [@CR8R] on the instance or you can open an issue in our [GitLab] repository.

  [support@cr8r.gg]: mailto:support@cr8r.gg
  [@CR8R]: https://cr8r.gg/@CR8R
  [GitLab]: https://gitlab.com/cr8r/community/-/issues/new?issue[title]=[Feedback]


As for this site (information documention & blog), this will be used to post updates on instance related incidents/changes/etc. The hope is to include a brief guide on how to use the different aspects of Mastodon for easy reference.

Will also be the place to view the most up to date rules, moderation style and generally everyone related to the instance as it evolves.

This is your community as much as ours, we want everyone to feel apart of it and help guide CR8R as it grows and develops.

Again, thank you all for joining and are looking forward to seeing the community interact with each other and posts of all your creations whatever they may be. Whatever you are doing (or not doing) hope you have a good time doing it.

Happy Hoildays & Happy New Year

Looking forward to what 2023 will bring.