---
date:
  created: 2024-02-18
  updated: 2024-02-18
description: >
  Incident: kuroneko discord spam bots
tags:
  - Incident
categories:
  - Incident
---

# Incident: kuroneko discord spam bots [ONGOING]

Latest updates regarding the aforementioned incident.

<!-- more -->

On the 16th of February, a wave of spambots hit a number of instances with new user registrations and constant spam posts.Luckily, we were active at the time they started joining our instance and measures were taken to minimise their effect.

Unfortunately it seems that there is a fair number of instances that as actively attended to and have taken (hopefully temporary) action against them.

``` title="instances (temporarily) limited:"
airwaves.social
cryptodon.lol
cunnin.me
educhat.social
ellis.social
library.love
makersocial.online
mastdn.social
mastodon.acm.org
mastodon.london
moth.social
naturalstate.social
nycity.social
onlybsds.com
portside.social
social.boom.army
terere.social
utter.online
vacatureforum.nl
witten.social
```

We will endevour to periodically review these and remove the limitations as soon as its apparent that they have a handle on things going forward.

Sorry for any inconvenience this may have caused and hope that it hasn't swayed you away from our instance or from the fediverse as a whole.






