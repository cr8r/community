---
title: How To Contribute
---

# Contribute To The Community

## Being an active member

Just by using the instance to follow, post and engage in conversations with other people are more than enough.

However, if you wish and have the capacity to contribute more, then there are other ways to help out

## The CR8R team

You could apply to become a voluntary member of the team that keeps it all running.

 - **Moderators**: Dealing with reports of questionable content &amp; reviewing current trends
 - **Admins**: Monitoring aspects of the infrastructure and mitigating any issues 

## Assisting with costs

If you'd like to donate to go towards the instance's running costs, then you can contribute by using our [ko-fi](https://ko-fi.com/cr8r) page.

All funds raised will solely be used to help cover the running & upgrade costs.

